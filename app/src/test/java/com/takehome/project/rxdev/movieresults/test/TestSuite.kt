package com.takehome.project.rxdev.movieresults.test

import com.takehome.project.rxdev.movieresults.test.tests.TestLocalDataSource
import com.takehome.project.rxdev.movieresults.test.tests.TestRemoteDataSource
import com.takehome.project.rxdev.movieresults.test.tests.TestRepository
import org.junit.runner.RunWith
import org.junit.runners.Suite
import org.junit.runners.Suite.*

@RunWith(Suite::class)
@SuiteClasses(TestLocalDataSource::class, TestRemoteDataSource::class, TestRepository::class)
class TestSuite