package com.takehome.project.rxdev.movieresults.test.implementations

import com.takehome.project.rxdev.movieresults.data.model.MovieCreditModel
import com.takehome.project.rxdev.movieresults.data.model.MovieModel
import com.takehome.project.rxdev.movieresults.data.source.remote.RemoteRxApi
import com.takehome.project.rxdev.movieresults.data.source.remote.RemoteRxDataSource
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MockRemoteDataSource(private val remoteApi : RemoteRxApi) : RemoteRxDataSource {

    override fun getTopRatedMovies() : Single<List<MovieModel>>? {
        return remoteApi.getTopRatedMovies()
            ?.flatMap { response ->
                Single.just(response.results)
            }
    }

    override fun getMovieCredits(movieId : Long) : Single<MovieCreditModel>? {
        return remoteApi.getMovieCredits(movieId)
    }
}