package com.takehome.project.rxdev.movieresults.test.tests

import com.takehome.project.rxdev.movieresults.data.model.*
import com.takehome.project.rxdev.movieresults.data.repository.MovieRepository
import com.takehome.project.rxdev.movieresults.data.repository.RxRepository
import com.takehome.project.rxdev.movieresults.data.source.local.LocalRxDataSource
import com.takehome.project.rxdev.movieresults.data.source.local.RoomDaoProvider
import com.takehome.project.rxdev.movieresults.data.source.remote.RemoteRxApi
import com.takehome.project.rxdev.movieresults.data.source.remote.RemoteRxDataSource
import com.takehome.project.rxdev.movieresults.test.implementations.MockLocalMovieDaoProvider
import com.takehome.project.rxdev.movieresults.test.implementations.MockLocalRxMovieDataSource
import com.takehome.project.rxdev.movieresults.test.implementations.MockRemoteApi
import com.takehome.project.rxdev.movieresults.test.implementations.MockRemoteDataSource
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class TestRepository {

    private lateinit var daoProvider: RoomDaoProvider
    private lateinit var remoteApi: RemoteRxApi
    private lateinit var localDataSource: LocalRxDataSource
    private lateinit var remoteDataSource: RemoteRxDataSource
    private lateinit var movieRepository : RxRepository

    private val responseModel = RetrofitMovieResponseModel()
    private val creditModel = arrayListOf<MovieCreditModel>()

    @Before
    fun setup() {

        daoProvider =
            MockLocalMovieDaoProvider()
        initMockRemoteApi()
        localDataSource =
            MockLocalRxMovieDataSource(
                daoProvider
            )
        remoteDataSource =
            MockRemoteDataSource(
                remoteApi
            )

        movieRepository =
            MovieRepository(
                localDataSource,
                remoteDataSource
            )
    }

    private fun initMockRemoteApi() {
        val movieTable = arrayListOf<MovieModel>()
        val castTable = arrayListOf<MovieCastModel>()
        val crewTable = arrayListOf<MovieCrewModel>()
        for (i in 0 until 10) {
            val movieModel = MovieModel().apply {
                id = i.toLong()
                title = "Movie at $i"
                overview = "Overview at $i"
                posterPath = null
                favorited = false
            }
            responseModel.results.add(movieModel)
            movieTable.add(movieModel)
            for (j in 0 until 10) {
                val castModel = MovieCastModel().apply {
                    movieId = i.toLong()
                    name = "Name at $j"
                    character = "Character at $j"
                    profilePath = null
                }
                castTable.add(castModel)
                val crewModel = MovieCrewModel().apply {
                    movieId = i.toLong()
                    name = "Name at $j"
                    job = "Job at $j"
                }
                crewTable.add(crewModel)
            }
            val movieCreditModel = MovieCreditModel().apply {
                id =  i.toLong()
                cast = castTable
                crew = crewTable
            }
            creditModel.add(movieCreditModel)
        }
        remoteApi =
            MockRemoteApi(
                responseModel,
                creditModel
            )
    }

    @Test
    fun test_get_top_rated_movies() {
        var list = listOf<MovieModel>()
        val index = 0
        movieRepository.getTopRatedMovies()
            ?.doOnSuccess {movieList ->
                list = movieList
            }
            ?.subscribe()
        assertEquals(responseModel.results[index].title, list[index].title)

    }

    @Test
    fun test_get_cached_movie() {
        var result = MovieModel()
        val expected = responseModel.results[0]
        //populate db first
        movieRepository.getTopRatedMovies()?.doOnSuccess {
            movieRepository.getCachedMovie(expected.id)?.doOnSuccess {movieModel ->
                result = movieModel
            }?.subscribe()
        }?.subscribe()
        assertEquals(expected.title, result.title)
    }

    @Test
    fun test_toggle_favorite() {
        //copy of model we want to change
        val expected = responseModel.results[0]
        val before = MovieModel().apply {
            id = expected.id
            favorited = !favorited
        }
        var after = MovieModel()
        assertFalse(expected.favorited)
        //populate db
        movieRepository.getTopRatedMovies()
            ?.doOnSuccess {
                Single.zip(movieRepository.toggleFavorite(before),
                    movieRepository.getCachedMovie(before.id),
                    BiFunction { updatedRows: Int, movieModel: MovieModel ->
                        return@BiFunction movieModel
                    }).doOnSuccess { movieModel -> after = movieModel }.subscribe()
            }
            ?.subscribe()
        assertNotEquals(expected.favorited, after.favorited)
    }

    @Test
    fun test_get_movie_credits() {
        val expectedCreditModel = creditModel[0]
        val movieModel = responseModel.results[0]
        var movieCreditModel : MovieCreditModel? = null
        movieRepository.getMovieCredits(movieModel.id)
            ?.doOnSuccess { model ->
                movieCreditModel = model
            }
            ?.subscribe()
        assertNotNull(movieCreditModel)
        assertEquals(expectedCreditModel.id, movieCreditModel!!.id)
    }
}