package com.takehome.project.rxdev.movieresults.test.implementations

import com.takehome.project.rxdev.movieresults.data.model.MovieCastModel
import com.takehome.project.rxdev.movieresults.data.model.MovieCreditModel
import com.takehome.project.rxdev.movieresults.data.model.MovieCrewModel
import com.takehome.project.rxdev.movieresults.data.model.MovieModel
import com.takehome.project.rxdev.movieresults.data.source.local.LocalMovieDao
import io.reactivex.Single

class MockMovieDao() : LocalMovieDao{

    constructor(movieList : List<MovieModel>, castList: List<MovieCastModel>, crewList: List<MovieCrewModel>) : this() {
        movieTable.addAll(movieList)
        castTable.addAll(castList)
        crewTable.addAll(crewList)
    }

    private val movieTable = arrayListOf<MovieModel>()
    private val castTable = arrayListOf<MovieCastModel>()
    private val crewTable = arrayListOf<MovieCrewModel>()

     override fun getAllMovies(): Single<List<MovieModel>> {
        return Single.just(movieTable)
    }

    override fun getMovie(id: Long): Single<MovieModel>? {
        return Single.just(movieTable.find { movieModel -> movieModel.id  == id })
    }

    /*override fun updateMovie(movieModel: MovieModel) {
        val index = movieTable.indexOf(movieTable.find { model -> model.id == movieModel.id })
        movieTable[index] = movieModel
    }*/

    override fun toggleFavorite(movieModel: MovieModel): Single<Int> {
        val index = movieTable.indexOf(movieTable.find { model -> model.id == movieModel.id })
        movieTable[index] = movieModel
        return Single.just(movieTable[index].id.toInt())
    }

    override fun insertMovies(movieList: List<MovieModel>?): Single<List<Long>>? {
        val list = arrayListOf<Long>()
        movieList?.forEach { movieModel ->
            val index = movieTable.indexOf(movieTable.find { model -> model.id == movieModel.id })
            if (index >= 0) {
                movieTable[index] = movieModel
                list.add(movieModel.id)
            }
            else {
                movieTable.add(movieModel)
                list.add(movieModel.id)
            }
        }
        return Single.just(list)
    }

    override fun insertCast(castList: List<MovieCastModel>?): Single<List<Long>>? {
        val list = arrayListOf<Long>()
        castList?.forEach { castModel ->
            val index = castTable.indexOf(castTable.find { model -> model.id == castModel.id })
            if (index >= 0) {
                castTable[index] = castModel
                list.add(castModel.id.toLong())
            }
            else {
                castTable.add(castModel)
                list.add(castModel.id.toLong())
            }
        }
        return Single.just(list)
    }

    override fun insertCrew(crewList: List<MovieCrewModel>?): Single<List<Long>>? {
        val list = arrayListOf<Long>()
        crewList?.forEach { crewModel ->
            val index = crewTable.indexOf(crewTable.find { model -> model.id == crewModel.id })
            if (index >= 0) {
                crewTable[index] = crewModel
                list.add(crewModel.id.toLong())
            }
            else {
                crewTable.add(crewModel)
                list.add(crewModel.id.toLong())
            }
        }
        return Single.just(list)
    }

    override fun getCastCredits(movieId: Long): Single<List<MovieCastModel>> {
        return Single.just(castTable.filter { castModel -> castModel.movieId == movieId })
    }

    override fun getCrewCredits(movieId: Long): Single<List<MovieCrewModel>> {
        return Single.just(crewTable.filter { crewModel -> crewModel.movieId == movieId })
    }
}