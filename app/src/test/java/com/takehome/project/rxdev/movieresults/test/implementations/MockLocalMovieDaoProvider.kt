package com.takehome.project.rxdev.movieresults.test.implementations

import com.takehome.project.rxdev.movieresults.data.source.local.LocalMovieDao
import com.takehome.project.rxdev.movieresults.data.source.local.RoomDaoProvider

class MockLocalMovieDaoProvider(private val movieDao: LocalMovieDao? = null) : RoomDaoProvider {

    override fun getLocalMovieDao(): LocalMovieDao {
        return movieDao ?: MockMovieDao()
    }
}