package com.takehome.project.rxdev.movieresults.test.implementations

import com.takehome.project.rxdev.movieresults.data.model.*
import com.takehome.project.rxdev.movieresults.data.source.remote.RemoteRxApi
import io.reactivex.Single

class MockRemoteApi(private val retrofitResponseModel: RetrofitMovieResponseModel,
                    private val movieCreditModel : List<MovieCreditModel>) : RemoteRxApi {

    private var NETWORK_DELAY : Long = 50 //MILLISECONDS

    override fun getTopRatedMovies(): Single<RetrofitMovieResponseModel> {
        Thread.sleep(NETWORK_DELAY) //simulate network response
        return Single.just(retrofitResponseModel)
    }

    override fun getMovieCredits(movieId: Long): Single<MovieCreditModel> {
        Thread.sleep(NETWORK_DELAY) //simulate network response
        val model = movieCreditModel.find { model -> model.id == movieId }
        return Single.just(model)
    }
}