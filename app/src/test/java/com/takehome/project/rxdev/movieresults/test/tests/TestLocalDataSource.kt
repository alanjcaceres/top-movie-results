package com.takehome.project.rxdev.movieresults.test.tests

import com.takehome.project.rxdev.movieresults.data.model.MovieCastModel
import com.takehome.project.rxdev.movieresults.data.model.MovieCreditModel
import com.takehome.project.rxdev.movieresults.data.model.MovieCrewModel
import com.takehome.project.rxdev.movieresults.data.model.MovieModel
import com.takehome.project.rxdev.movieresults.data.source.local.LocalMovieDao
import com.takehome.project.rxdev.movieresults.data.source.local.LocalRxDataSource
import com.takehome.project.rxdev.movieresults.data.source.local.LocalRxMovieDataSource
import com.takehome.project.rxdev.movieresults.data.source.local.RoomDaoProvider
import com.takehome.project.rxdev.movieresults.test.implementations.MockLocalMovieDaoProvider
import com.takehome.project.rxdev.movieresults.test.implementations.MockMovieDao
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Before
import org.junit.Test
import kotlin.random.Random

class TestLocalDataSource {

    private lateinit var daoProvider: RoomDaoProvider
    private lateinit var localDataSource: LocalRxDataSource
    private lateinit var mockMovieDao: LocalMovieDao

    private val movieTable = arrayListOf<MovieModel>()
    private val castTable = arrayListOf<MovieCastModel>()
    private val crewTable = arrayListOf<MovieCrewModel>()

    @Before
    fun setup() {
        initMockTables()
        daoProvider =
            MockLocalMovieDaoProvider(mockMovieDao)

        localDataSource =
            LocalRxMovieDataSource(
                daoProvider
            )
    }

    private fun initMockTables() {
        movieTable.clear()
        castTable.clear()
        crewTable.clear()
        for (i in 0 until 10) {
            val movieModel = MovieModel().apply {
                id = i.toLong()
                title = "Movie at $i"
                overview = "Overview at $i"
                posterPath = null
                favorited = false
            }
            movieTable.add(movieModel)
            for (j in 0 until 10) {
                val castModel = MovieCastModel().apply {
                    id = Random(j).nextLong(j.toLong(), 1000)
                    movieId = i.toLong()
                    name = "Name at $j"
                    character = "Character at $j"
                    profilePath = null
                }
                castTable.add(castModel)
                val crewModel = MovieCrewModel().apply {
                    id = Random(castModel.id).nextLong(castModel.id, 1000)
                    movieId = i.toLong()
                    name = "Name at $j"
                    job = "Job at $j"
                }
                crewTable.add(crewModel)
            }
        }
        mockMovieDao = MockMovieDao(movieTable,castTable,crewTable)
    }

    @Test
    fun test_get_top_rated_movies(){
        var actualList = listOf<MovieModel>()
        assertNotEquals(movieTable, actualList)
        localDataSource.getTopRatedMovies()
            ?.doOnSuccess {movieList ->
            actualList = movieList
        }
            ?.subscribe()
        assertEquals(movieTable, actualList)
    }

    @Test
    fun test_get_movie() {
        val expectedMovie = movieTable[0]
        var actualMovie = MovieModel()
        assertNotEquals(expectedMovie, actualMovie)
        localDataSource.getMovie(expectedMovie.id)
            ?.doOnSuccess { movie ->
            actualMovie = movie
        }
            ?.subscribe()
        assertEquals(expectedMovie, actualMovie)
    }

    @Test
    fun test_get_movie_credits() {
        val expectedMovie = movieTable[0]
        val expectedMovieCredits = MovieCreditModel().apply {
            id = expectedMovie.id
            cast = castTable.filter { castModel -> castModel.movieId == id }
            crew = crewTable.filter { crewModel -> crewModel.movieId == id }
        }
        var actualMovieCredits = MovieCreditModel()
        assertNotEquals(expectedMovieCredits.id, actualMovieCredits.id)
        localDataSource.getMovieCredits(expectedMovie.id)
            ?.doOnSuccess {
                movieCreditModel ->
            actualMovieCredits = movieCreditModel
        }
            ?.subscribe()
        assertEquals(expectedMovieCredits.id, actualMovieCredits.id)
    }

    @Test
    fun test_get_cast_credits() {
        val expectedMovie = movieTable[0]
        val expectedCastList = castTable.filter { castModel ->
            castModel.movieId == expectedMovie.id }
        var actualCastList = listOf<MovieCastModel>()
        assertNotEquals(expectedCastList, actualCastList)
        localDataSource.getCastCredits(expectedMovie.id)
            ?.doOnSuccess { castList ->
            actualCastList = castList
        }
            ?.subscribe()
        assertEquals(expectedCastList, actualCastList)
    }

    @Test
    fun test_get_crew_credits() {
        val expectedMovie = movieTable[0]
        val expectedCrewList = crewTable.filter {
                crewModel -> crewModel.movieId == expectedMovie.id }
        var actualCrewList = listOf<MovieCrewModel>()
        assertNotEquals(expectedCrewList, actualCrewList)
        localDataSource.getCrewCredits(expectedMovie.id)
            ?.doOnSuccess { crewList ->
            actualCrewList = crewList
        }
            ?.subscribe()
        assertEquals(expectedCrewList, actualCrewList)
    }

    @Test
    fun test_toggle_favorite(){
        val expectedMovie = movieTable[0]
        val toggledMovie = MovieModel().apply {
            id = expectedMovie.id
            title = expectedMovie.title
            favorited = !expectedMovie.favorited
        }
        var resultingMovie = MovieModel()
        assertEquals(expectedMovie.favorited, resultingMovie.favorited)
        Single.zip(localDataSource.toggleFavorite(toggledMovie),
            localDataSource.getMovie(toggledMovie.id), BiFunction {
                    updatedRows : Int, movieModel : MovieModel ->
                return@BiFunction movieModel
            })
            .doOnSuccess { movieModel ->
            resultingMovie = movieModel
        }
            .subscribe()
        assertNotEquals(expectedMovie.favorited, resultingMovie.favorited)
    }

    @Test
    fun test_cache_movie_models(){
        val list = arrayListOf<MovieModel>()
        val expectedIdList = arrayListOf<Long>()
        for (i in 19 until 30) {
            list.add(MovieModel().apply {
                id = i.toLong()
                title = "Movie at $i"
                overview = "Overview at $i"
            })
            expectedIdList.add(i.toLong())
        }
        var actualIdList = listOf<Long>()
        assertNotEquals(expectedIdList, actualIdList)
        localDataSource.cacheMovieModels(list)
            ?.doOnSuccess { movieIdList ->
            actualIdList = movieIdList
        }
            ?.subscribe()
        assertEquals(expectedIdList, actualIdList)
    }

    @Test
    fun test_cache_movie_credits(){
        val expectedIdList = arrayListOf<Long>()
        val movieModel = movieTable[0]
        val creditModel = MovieCreditModel().apply {
            id = movieModel.id
            cast = castTable.filter { castList -> castList.movieId == movieModel.id }
            cast.forEach { castModel ->
                expectedIdList.add(castModel.id.toLong())
            }
            crew = crewTable.filter { crewList -> crewList.movieId == movieModel.id }
            crew.forEach { crewModel ->
                expectedIdList.add(crewModel.id.toLong())
            }
        }
        var actualIdList = listOf<Long>()
        assertNotEquals(expectedIdList, actualIdList)
        localDataSource.cacheMovieCredits(creditModel)
            ?.doOnSuccess { creditIdList ->
            actualIdList = creditIdList
        }
            ?.subscribe()
        assertEquals(expectedIdList, actualIdList)
    }
}