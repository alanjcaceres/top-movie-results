package com.takehome.project.rxdev.movieresults.test.tests

import com.takehome.project.rxdev.movieresults.data.model.*
import com.takehome.project.rxdev.movieresults.data.source.remote.RemoteRxApi
import com.takehome.project.rxdev.movieresults.data.source.remote.RemoteRxDataSource
import com.takehome.project.rxdev.movieresults.data.source.remote.RemoteRxMovieDataSource
import com.takehome.project.rxdev.movieresults.test.implementations.MockRemoteApi
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import kotlin.random.Random

class TestRemoteDataSource {

    private val remoteApi by lazy {
        MockRemoteApi(responseModel,creditModelList)
    }
    private val remoteDataSource by lazy {
        RemoteRxMovieDataSource(remoteApi)
    }

    private val responseModel = RetrofitMovieResponseModel()
    private val creditModelList = arrayListOf<MovieCreditModel>()

    @Before
    fun setup() {
        initMockRemoteApi()
    }

    private fun initMockRemoteApi() {
        val movieTable = arrayListOf<MovieModel>()
        val castTable = arrayListOf<MovieCastModel>()
        val crewTable = arrayListOf<MovieCrewModel>()
        for (i in 0 until 10) {
            val movieModel = MovieModel().apply {
                id = i.toLong()
                title = "Movie at $i"
                overview = "Overview at $i"
                posterPath = null
                favorited = false
            }
            responseModel.results.add(movieModel)
            movieTable.add(movieModel)
            for (j in 0 until 10) {
                val castModel = MovieCastModel().apply {
                    id = Random(j).nextLong(j.toLong(), 1000)
                    movieId = i.toLong()
                    name = "Name $id"
                    character = "Character $id"
                    profilePath = null
                }
                castTable.add(castModel)
                val crewModel = MovieCrewModel().apply {
                    id = Random(castModel.id).nextLong(castModel.id, 1000)
                    movieId = i.toLong()
                    name = "Name $id"
                    job = "Job $id"
                }
                crewTable.add(crewModel)
            }
            val movieCreditModel = MovieCreditModel().apply {
                id =  i.toLong()
                cast = castTable
                crew = crewTable
            }
            creditModelList.add(movieCreditModel)
        }
    }

    @Test
    fun test_get_top_rated_movies() {
        val expectedList = responseModel.results
        var actualList = listOf<MovieModel>()
        remoteDataSource.getTopRatedMovies()?.doOnSuccess { movieList ->
            actualList = movieList
        }?.subscribe()
        assertEquals(expectedList, actualList)
    }

    @Test
    fun test_get_movie_credits() {
        val expectedMovie = responseModel.results[0]
        val expectedModel = creditModelList.find { model -> model.id == expectedMovie.id }
        var actualCreditModel = MovieCreditModel()
        remoteDataSource.getMovieCredits(expectedMovie.id)?.doOnSuccess { movieCreditModel ->
            actualCreditModel = movieCreditModel
        }?.subscribe()
        assertEquals(expectedModel, actualCreditModel)
    }
}