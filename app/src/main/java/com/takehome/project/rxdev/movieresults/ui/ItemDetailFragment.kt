package com.takehome.project.rxdev.movieresults.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.takehome.project.rxdev.movieresults.data.model.MovieCastModel
import com.takehome.project.rxdev.movieresults.data.model.MovieCrewModel
import com.takehome.project.rxdev.movieresults.data.model.MovieModel
import com.takehome.project.rxdev.movieresults.data.model.viewmodel.MoviesViewModelFactory
import com.takehome.project.rxdev.movieresults.data.model.viewmodel.TopRatedMoviesViewModel
import com.takehome.project.rxdev.movieresults.data.repository.RxRepository
import com.takehome.project.rxdev.movieresults.databinding.ItemDetailBinding
import com.takehome.project.rxdev.movieresults.ui.adapters.MovieCreditAdapter
import com.takehome.project.rxdev.movieresults.ui.adapters.delegates.BaseAdapterInteractionDelegate
import kotlinx.android.synthetic.main.item_detail.*

class ItemDetailFragment : Fragment() {

    private var viewModel : TopRatedMoviesViewModel? = null
    private lateinit var movieRepository: RxRepository
    private lateinit var binding: ItemDetailBinding
    private lateinit var interactionDelegate: BaseAdapterInteractionDelegate<MovieModel>
    private lateinit var movieCastAdapter : MovieCreditAdapter<MovieCastModel>
    private lateinit var movieCrewAdapter : MovieCreditAdapter<MovieCrewModel>
    companion object {
        fun newInstance(movieRepository: RxRepository, interactionDelegate: BaseAdapterInteractionDelegate<MovieModel>) : Fragment {
            val itemDetailFragment = ItemDetailFragment()
            itemDetailFragment.movieRepository = movieRepository
            itemDetailFragment.interactionDelegate = interactionDelegate
            return itemDetailFragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = activity?.run {
            ViewModelProvider(this, MoviesViewModelFactory(movieRepository))
                .get(TopRatedMoviesViewModel::class.java)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = ItemDetailBinding.inflate(inflater,container,false)
        binding.movieModel = viewModel?.selectedMovie?.value
        binding.movieCreditModel = viewModel?.movieCredits?.value

        movieCastAdapter =
            MovieCreditAdapter(
                binding.movieCreditModel?.cast
            )
        movieCrewAdapter =
            MovieCreditAdapter(
                binding.movieCreditModel?.crew
            )

        binding.rvCast.adapter = movieCastAdapter
        binding.rvCrew.adapter = movieCrewAdapter

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        iv_favorite_button.setOnClickListener {
            viewModel?.toggleFavorite(viewModel?.selectedMovie?.value)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel?.selectedMovie?.observe(viewLifecycleOwner, Observer { selectedMovie ->
            binding.movieModel = selectedMovie

        })
        viewModel?.movieCredits?.observe(viewLifecycleOwner, Observer { movieCredits ->
            movieCastAdapter.updateList(movieCredits?.cast)
            movieCrewAdapter.updateList(movieCredits?.crew)
        })
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putBoolean("selectedMovie", viewModel?.selectedMovie?.value != null)
        super.onSaveInstanceState(outState)
    }
}