package com.takehome.project.rxdev.movieresults.ui.adapters.viewholders

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.takehome.project.rxdev.movieresults.ui.adapters.delegates.BaseAdapterInteractionDelegate

abstract class BaseViewHolder<T>(viewDataBinding: ViewDataBinding) :
    RecyclerView.ViewHolder(viewDataBinding.root) {

    var interactionDelegate : BaseAdapterInteractionDelegate<T>? = null

    abstract fun bind(model: T?)


}