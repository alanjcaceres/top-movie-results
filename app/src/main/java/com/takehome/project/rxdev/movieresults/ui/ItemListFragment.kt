package com.takehome.project.rxdev.movieresults.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.SimpleItemAnimator
import androidx.transition.TransitionInflater
import com.takehome.project.rxdev.movieresults.R
import com.takehome.project.rxdev.movieresults.data.model.MovieModel
import com.takehome.project.rxdev.movieresults.data.model.viewmodel.MoviesViewModelFactory
import com.takehome.project.rxdev.movieresults.data.model.viewmodel.TopRatedMoviesViewModel
import com.takehome.project.rxdev.movieresults.data.repository.RxRepository
import com.takehome.project.rxdev.movieresults.ui.adapters.MovieListAdapter
import com.takehome.project.rxdev.movieresults.ui.adapters.delegates.BaseAdapterInteractionDelegate
import kotlinx.android.synthetic.main.item_list.*


class ItemListFragment : Fragment() {

    private var viewModel : TopRatedMoviesViewModel? =  null
    private lateinit var movieRepository: RxRepository
    private lateinit var interactionDelegate: BaseAdapterInteractionDelegate<MovieModel>
    companion object {
        fun newInstance(movieRepository: RxRepository, interactionDelegate: BaseAdapterInteractionDelegate<MovieModel>) : Fragment {
            val itemListFragment = ItemListFragment()
            itemListFragment.movieRepository = movieRepository
            itemListFragment.interactionDelegate = interactionDelegate
            return itemListFragment
        }
    }

    private val movieListAdapter by lazy {
        MovieListAdapter(
            interactionDelegate
        ).apply { setHasStableIds(true) }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        exitTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.fade)
        viewModel = activity?.run {
            ViewModelProvider(this, MoviesViewModelFactory(movieRepository))
                .get(TopRatedMoviesViewModel::class.java)
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return LayoutInflater.from(context).inflate(R.layout.item_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rv_item_list.adapter = movieListAdapter

        rv_item_list.itemAnimator.apply {
            (this as SimpleItemAnimator).supportsChangeAnimations = false
        }
    }

    @SuppressLint("CheckResult")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel?.movies?.observe(viewLifecycleOwner, Observer { movieList ->
            movieListAdapter.updateMovieList(movieList)
        })

        viewModel?.initMovieList()
            ?.subscribe {_, error ->
                error?.let {
                    try {
                        throw it
                    }catch (e: Throwable) {
                        AlertDialog.Builder(context!!)
                            .setMessage(R.string.network_connection_dialog)
                            .setNeutralButton("OK") { dialogInterface, _ ->
                                dialogInterface.dismiss()
                                activity?.finish()
                            }.create()
                            .show()
                    }
                }
            }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putBoolean("selectedMovie", viewModel?.selectedMovie?.value != null)
        super.onSaveInstanceState(outState)
    }
}