package com.takehome.project.rxdev.movieresults.data.source.remote

import androidx.databinding.ObservableBoolean
import com.takehome.project.rxdev.movieresults.BuildConfig
import com.takehome.project.rxdev.movieresults.data.contracts.RetrofitMovieInterface
import com.takehome.project.rxdev.movieresults.data.model.MovieCreditModel
import com.takehome.project.rxdev.movieresults.data.model.RetrofitMovieResponseModel
import io.reactivex.Single
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class RemoteMovieApi(private val observable: ObservableBoolean) :
    RemoteRxApi {

    private var hasNetwork : Boolean = observable.get()
    init {
        observable.addOnPropertyChangedCallback(
            object : androidx.databinding.Observable.OnPropertyChangedCallback() {
                override fun onPropertyChanged(
                    sender: androidx.databinding.Observable?,
                    propertyId: Int) {
                    hasNetwork = observable.get()
                }
            })
    }

    private val movieApi by lazy {
        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor()
                .apply { level = HttpLoggingInterceptor.Level.BODY })
            .addInterceptor(object : Interceptor {
                override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
                    val original = chain.request()
                    val url = original.url
                    val newUrl = url.newBuilder()
                        .addQueryParameter("api_key", BuildConfig.API_KEY)
                        .build()
                    val builder = original.newBuilder().url(newUrl)
                    val request = builder.build()

                    return chain.proceed(request)
                }
            })
            .build()
        Retrofit.Builder()
            .baseUrl(BuildConfig.API_BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(RetrofitMovieInterface::class.java)
    }

    override fun getTopRatedMovies(): Single<RetrofitMovieResponseModel>? {
        return if (hasNetwork)  movieApi.getTopRatedMovies()
        else Single.just(RetrofitMovieResponseModel())
    }

    override fun getMovieCredits(movieId: Long): Single<MovieCreditModel>? {
        return if (hasNetwork) movieApi.getMovieCredits(movieId)
        else Single.just(MovieCreditModel())
    }
}