package com.takehome.project.rxdev.movieresults.data.source.remote

import android.util.Log
import com.takehome.project.rxdev.movieresults.data.model.MovieCreditModel
import com.takehome.project.rxdev.movieresults.data.model.MovieModel
import io.reactivex.Single


class RemoteRxMovieDataSource(private val remoteApi: RemoteRxApi) :
    RemoteRxDataSource {

    override fun getTopRatedMovies() : Single<List<MovieModel>>? {
        return remoteApi.getTopRatedMovies()
            ?.flatMap { response ->
                Single.just(response.results)
            }
    }

    override fun getMovieCredits(movieId : Long) : Single<MovieCreditModel>? {
        return remoteApi.getMovieCredits(movieId)
    }

}