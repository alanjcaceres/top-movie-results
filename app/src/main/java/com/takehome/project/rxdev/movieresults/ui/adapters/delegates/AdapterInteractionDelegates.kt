package com.takehome.project.rxdev.movieresults.ui.adapters.delegates

import android.view.View

interface AdapterInteractionDelegates {

    interface Select<T>{
        fun onAdapterItemSelected(position: Int, item: T?)
    }

    interface Favorite<T> {
        fun onAdapterItemFavorited(position: Int, item: T?)
    }

}