package com.takehome.project.rxdev.movieresults.data.model

interface CreditModel {

    fun getCreditImagePath() : String?
    fun getCreditName() : String
    fun getCreditTitle() : String?

}