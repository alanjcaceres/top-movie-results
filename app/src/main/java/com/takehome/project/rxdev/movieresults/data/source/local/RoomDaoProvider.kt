package com.takehome.project.rxdev.movieresults.data.source.local

interface RoomDaoProvider {

    fun getLocalMovieDao() : LocalMovieDao

}