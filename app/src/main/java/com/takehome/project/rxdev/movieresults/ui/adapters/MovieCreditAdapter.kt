package com.takehome.project.rxdev.movieresults.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.takehome.project.rxdev.movieresults.data.model.CreditModel
import com.takehome.project.rxdev.movieresults.databinding.CastCrewCardBinding
import com.takehome.project.rxdev.movieresults.ui.adapters.viewholders.MovieCreditViewHolder

class MovieCreditAdapter<T : CreditModel>(private val list : List<T>?) : RecyclerView.Adapter<MovieCreditViewHolder<T>>() {

    private lateinit var binding : CastCrewCardBinding
    private var adapterList = arrayListOf<T>()

    init {
        list?.toCollection(adapterList)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieCreditViewHolder<T> {
        binding = CastCrewCardBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MovieCreditViewHolder(
            binding
        )
    }

    override fun getItemCount() = adapterList.size

    override fun onBindViewHolder(holder: MovieCreditViewHolder<T>, position: Int) {
        holder.bind(adapterList[position])
    }

    fun updateList(updatedList : List<T>?) {
        adapterList.clear()
        updatedList?.toCollection(adapterList)
    }

}