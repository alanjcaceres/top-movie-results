package com.takehome.project.rxdev.movieresults.data.source.local

import androidx.room.*
import com.takehome.project.rxdev.movieresults.data.model.MovieCastModel
import com.takehome.project.rxdev.movieresults.data.model.MovieCrewModel
import com.takehome.project.rxdev.movieresults.data.model.MovieModel
import io.reactivex.Single

@Dao
interface LocalMovieDao {

    @Query("SELECT * FROM movie_models")
     fun getAllMovies() : Single<List<MovieModel>>

    @Query("SELECT * FROM movie_models WHERE id = :id")
     fun getMovie(id: Long) : Single<MovieModel>?

    @Update
     fun toggleFavorite(movieModel: MovieModel) : Single<Int>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
     fun insertMovies(movieList : List<MovieModel>?) : Single<List<Long>>?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
     fun insertCast(castList : List<MovieCastModel>?) : Single<List<Long>>?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
     fun insertCrew(crewList : List<MovieCrewModel>?) : Single<List<Long>>?

    @Query("SELECT * FROM movie_cast_model WHERE movie_cast_model.movieId = :movieId")
     fun getCastCredits(movieId : Long) : Single<List<MovieCastModel>>

    @Query("SELECT * FROM movie_crew_model WHERE movie_crew_model.movieId = :movieId")
     fun getCrewCredits(movieId : Long) : Single<List<MovieCrewModel>>

}