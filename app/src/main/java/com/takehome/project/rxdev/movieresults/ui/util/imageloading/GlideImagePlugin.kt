package com.takehome.project.rxdev.movieresults.ui.util.imageloading

import android.widget.ImageView
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.takehome.project.rxdev.movieresults.BuildConfig
import com.takehome.project.rxdev.movieresults.R

class GlideImagePlugin : ImagePlugin{

    override fun loadMovieCreditImage(imageVisitable: ImageVisitable) {
        val imageViewRef : ImageView? = imageVisitable.imageViewRef.get()

        imageViewRef?.apply {
            val drawable = ContextCompat.getDrawable(
                imageViewRef.context,
                R.drawable.ic_baseline_person_24)
            val requestOptions = RequestOptions()
                .centerCrop()
                .placeholder(drawable)
                .error(drawable)
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
            val requestManager = Glide.with(this)
            imageVisitable.imagePath?.let {
                val path = "${BuildConfig.IMAGE_BASE_URL}${imageVisitable.imagePath}"
                requestManager.load(path)
                    .apply(requestOptions)
                    .into(this)
            } ?: requestManager.load(
                drawable)
                .centerInside()
                .into(this)
        }
    }

    override fun loadMoviePosterImage(imageVisitable: ImageVisitable) {
        val imageViewRef : ImageView? = imageVisitable.imageViewRef.get()

        imageViewRef?.apply {
            val requestManager = Glide.with(this)
            val drawable = ContextCompat
                .getDrawable(imageViewRef.context,
                    R.drawable.ic_baseline_local_movies_24)
            val requestOptions = RequestOptions()
                .centerCrop()
                .error(drawable)
                .override(800, 1080)
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
            imageVisitable.imagePath?.let {
                val path = "${BuildConfig.IMAGE_BASE_URL}${imageVisitable.imagePath}"
                requestManager.load(path)
                    .apply(requestOptions)
                    .into(this)
            } ?: requestManager
                .load(drawable)
                .into(this)
        }
    }

}