package com.takehome.project.rxdev.movieresults.ui.adapters.delegates

interface BaseAdapterInteractionDelegate<T> :
    AdapterInteractionDelegates.Select<T>, AdapterInteractionDelegates.Favorite<T>