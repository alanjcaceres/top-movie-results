package com.takehome.project.rxdev.movieresults.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "movie_crew_model")
class MovieCrewModel :
    CreditModel {

    var movieId = -1L

    @SerializedName("credit_id")
    var creditId = ""

    var department = ""

    var gender : Int? = null

    @PrimaryKey
    @ColumnInfo(name = "movieCrewId")
    var id = -1L

    var job = ""

    var name = ""

    @SerializedName("profile_path")
    var profilePath : String? = null

    override fun getCreditImagePath(): String? {
        return profilePath
    }

    override fun getCreditName(): String {
        return name
    }

    override fun getCreditTitle(): String? {
        return job
    }

    override fun toString(): String {
        return "$name - $profilePath\n$job : $department"
    }
}