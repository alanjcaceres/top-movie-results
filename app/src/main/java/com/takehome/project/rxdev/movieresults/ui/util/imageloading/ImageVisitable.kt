package com.takehome.project.rxdev.movieresults.ui.util.imageloading

import android.widget.ImageView
import com.takehome.project.rxdev.movieresults.ui.util.imageloading.ImagePlugin
import java.lang.ref.WeakReference

interface ImageVisitable {

    val imageViewRef : WeakReference<ImageView>
    val imagePath : String?

    fun loadImageWith(imagePlugin: ImagePlugin)

}