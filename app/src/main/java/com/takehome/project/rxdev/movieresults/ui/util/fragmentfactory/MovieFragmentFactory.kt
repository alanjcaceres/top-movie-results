package com.takehome.project.rxdev.movieresults.ui.util.fragmentfactory

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import com.takehome.project.rxdev.movieresults.data.model.MovieModel
import com.takehome.project.rxdev.movieresults.data.repository.RxRepository
import com.takehome.project.rxdev.movieresults.ui.ItemDetailFragment
import com.takehome.project.rxdev.movieresults.ui.ItemListFragment
import com.takehome.project.rxdev.movieresults.ui.adapters.delegates.BaseAdapterInteractionDelegate

class MovieFragmentFactory(private val movieRepository: RxRepository,
                           private val interactionDelegate: BaseAdapterInteractionDelegate<MovieModel>
) : FragmentFactory() {

    override fun instantiate(classLoader: ClassLoader, className: String): Fragment {
        val clazz = loadFragmentClass(classLoader, className)
        return when {
            clazz.isAssignableFrom(ItemListFragment::class.java) ->
                ItemListFragment.newInstance(movieRepository, interactionDelegate)
            clazz.isAssignableFrom(ItemDetailFragment::class.java) ->
                ItemDetailFragment.newInstance(movieRepository, interactionDelegate)
            else -> super.instantiate(classLoader, className)
        }
    }
}