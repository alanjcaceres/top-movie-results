package com.takehome.project.rxdev.movieresults.data.model.viewmodel

import android.annotation.SuppressLint
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.takehome.project.rxdev.movieresults.data.model.MovieCreditModel
import com.takehome.project.rxdev.movieresults.data.model.MovieModel
import com.takehome.project.rxdev.movieresults.data.repository.RxRepository
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.lang.Exception

class TopRatedMoviesViewModel(private val movieRepository: RxRepository) : ViewModel() {

    private val _selectedMovie = MutableLiveData<MovieModel>()
    val selectedMovie : LiveData<MovieModel>
        get() = _selectedMovie

    private val _movieCredits = MutableLiveData<MovieCreditModel>()
    val movieCredits: LiveData<MovieCreditModel>
        get() = _movieCredits

    private val _movies = MutableLiveData<List<MovieModel>>()
    val movies : LiveData<List<MovieModel>>
        get() = _movies

    fun initMovieList() : Single<List<MovieModel>>?{
        return updateMovieList()
    }

    @SuppressLint("CheckResult")
    private fun updateMovieList() : Single<List<MovieModel>>? {
        return movieRepository.getTopRatedMovies()
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.doOnSuccess { movieList ->
                _movies.value = movieList
            }
    }

    fun toggleFavorite(movieModel: MovieModel?) {

        movieModel?.let {
            movieRepository.toggleFavorite(movieModel.apply { favorited = !favorited })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { updatedRows, error ->
                    try {
                        throw error
                    }catch (error : Exception) {}
                    updatedRows?.let { _ ->
                        updateMovieList()
                        updateSelectedMovie(movieModel)
                    }
                }
        }
    }
    private fun updateSelectedMovie(movieModel: MovieModel) {
        selectedMovie.value?.let { selectedMovie ->
            if (movieModel == selectedMovie){
                _selectedMovie.value = movieModel
            }
        }
    }

    @SuppressLint("CheckResult")
    private fun getMovieCredits(movieId: Long?) : Single<MovieCreditModel>?{
        return movieId?.run {
            movieRepository.getMovieCredits(movieId)
        } ?: Single.error(IllegalArgumentException("No movie found with id $movieId"))
    }

    @SuppressLint("CheckResult")
    fun getSelectedMovieDetails(movieModel: MovieModel, observer : ((MovieCreditModel?, Throwable?)-> Unit)) {
        _selectedMovie.value = movieModel
        getMovieCredits(movieModel.id)
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.doOnSuccess { _movieCredits.value = it }
            ?.doOnError { _movieCredits.value = MovieCreditModel() }
            ?.subscribe(observer)
    }

    fun clearSelectedMovie() {
        _selectedMovie.value = null
        _movieCredits.value = MovieCreditModel()
    }
}

