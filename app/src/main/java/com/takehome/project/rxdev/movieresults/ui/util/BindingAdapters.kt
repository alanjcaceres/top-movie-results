package com.takehome.project.rxdev.movieresults.ui.util

import android.view.View
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.takehome.project.rxdev.movieresults.R
import com.takehome.project.rxdev.movieresults.ui.util.imageloading.GlideImagePlugin
import com.takehome.project.rxdev.movieresults.ui.util.imageloading.MovieCreditImageLoader
import com.takehome.project.rxdev.movieresults.ui.util.imageloading.MoviePosterImageLoader

private val imagePlugin = GlideImagePlugin()

@BindingAdapter("posterImage")
fun setPosterImage(imageView: ImageView, imagePath: String?) {
    MoviePosterImageLoader(
        imageView,
        imagePath
    ).loadImageWith(imagePlugin)
}

@BindingAdapter("favorited")
fun setFavorited(imageView: ImageView, isFavorited: Boolean) {
    when(isFavorited) {
        true -> imageView.setImageDrawable(
            ContextCompat.getDrawable(
                imageView.context, R.drawable.ic_baseline_favorite_24))

        false -> imageView.setImageDrawable(
            ContextCompat.getDrawable(
                imageView.context, R.drawable.ic_baseline_favorite_border_24))
    }
}

@BindingAdapter("creditImage")
fun setCreditImage(imageView: ImageView, imagePath: String?) {
    MovieCreditImageLoader(
        imageView,
        imagePath
    ).loadImageWith(imagePlugin)
}

@BindingAdapter("visibility")
fun setVisibility(view: View, hasContent : Boolean) {
    when(hasContent) {
        true -> view.visibility = View.VISIBLE
        false -> view.visibility = View.GONE
    }
}