package com.takehome.project.rxdev.movieresults.data.repository

import com.takehome.project.rxdev.movieresults.data.model.MovieCreditModel
import com.takehome.project.rxdev.movieresults.data.model.MovieModel
import io.reactivex.Single

interface RxRepository {

    fun getTopRatedMovies() : Single<List<MovieModel>>?
    fun getMovieCredits(movieId: Long) : Single<MovieCreditModel>?
    fun getCachedMovie(movieId: Long) : Single<MovieModel>?
    fun toggleFavorite(movieModel: MovieModel) : Single<Int>

}