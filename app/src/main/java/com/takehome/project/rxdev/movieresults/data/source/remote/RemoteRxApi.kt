package com.takehome.project.rxdev.movieresults.data.source.remote

import com.takehome.project.rxdev.movieresults.data.model.MovieCreditModel
import com.takehome.project.rxdev.movieresults.data.model.RetrofitMovieResponseModel
import io.reactivex.Single
import okhttp3.Callback

interface RemoteRxApi {
    fun getTopRatedMovies() : Single<RetrofitMovieResponseModel>?
    fun getMovieCredits(movieId: Long) : Single<MovieCreditModel>?
}