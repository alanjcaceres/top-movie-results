package com.takehome.project.rxdev.movieresults.data.contracts

import com.takehome.project.rxdev.movieresults.data.model.MovieCreditModel
import com.takehome.project.rxdev.movieresults.data.model.RetrofitMovieResponseModel
import io.reactivex.Single
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Path


interface RetrofitMovieInterface {

    @GET("3/movie/top_rated")
    fun getTopRatedMovies() : Single<RetrofitMovieResponseModel>

    @GET("3/movie/{movie_id}/credits")
    fun getMovieCredits(@Path("movie_id") movieId: Long) : Single<MovieCreditModel>
}