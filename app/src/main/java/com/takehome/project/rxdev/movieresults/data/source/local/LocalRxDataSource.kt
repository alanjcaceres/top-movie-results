package com.takehome.project.rxdev.movieresults.data.source.local

import com.takehome.project.rxdev.movieresults.data.model.MovieCastModel
import com.takehome.project.rxdev.movieresults.data.model.MovieCreditModel
import com.takehome.project.rxdev.movieresults.data.model.MovieCrewModel
import com.takehome.project.rxdev.movieresults.data.model.MovieModel
import io.reactivex.Single


interface LocalRxDataSource {

    fun getTopRatedMovies() : Single<List<MovieModel>>?
    fun getMovie(movieId: Long) : Single<MovieModel>?
    fun getMovieCredits(movieId: Long) : Single<MovieCreditModel>?
    fun getCastCredits(movieId: Long) : Single<List<MovieCastModel>>?
    fun getCrewCredits(movieId: Long) : Single<List<MovieCrewModel>>?

    fun toggleFavorite(movieModel: MovieModel): Single<Int>

    fun cacheMovieModels(movieList: List<MovieModel>?) : Single<List<Long>>?
    fun cacheMovieCredits(movieCredits : MovieCreditModel) : Single<List<Long>>?

}