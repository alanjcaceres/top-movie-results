package com.takehome.project.rxdev.movieresults.data.source.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.takehome.project.rxdev.movieresults.data.model.MovieCastModel
import com.takehome.project.rxdev.movieresults.data.model.MovieCrewModel
import com.takehome.project.rxdev.movieresults.data.model.MovieModel

@Database(entities = [MovieModel::class, MovieCrewModel::class, MovieCastModel::class],
    version = 1, exportSchema = false)
abstract class LocalMovieDaoProvider : RoomDatabase(),
    RoomDaoProvider {

    companion object {
        private var instance : RoomDaoProvider? = null

        fun getRoomDatabase(context: Context) : RoomDaoProvider {
            return instance?.let { it } ?: let {
                instance = Room.databaseBuilder(context.applicationContext,
                LocalMovieDaoProvider::class.java, "LocalMovies.db").build()
                instance!!
            }
        }
    }
}