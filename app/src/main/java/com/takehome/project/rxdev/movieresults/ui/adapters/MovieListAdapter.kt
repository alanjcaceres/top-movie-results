package com.takehome.project.rxdev.movieresults.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SortedList
import androidx.recyclerview.widget.SortedListAdapterCallback
import com.takehome.project.rxdev.movieresults.data.model.MovieModel
import com.takehome.project.rxdev.movieresults.databinding.ItemListContentBinding
import com.takehome.project.rxdev.movieresults.ui.adapters.delegates.BaseAdapterInteractionDelegate
import com.takehome.project.rxdev.movieresults.ui.adapters.viewholders.MovieViewHolder

class MovieListAdapter(private val adapterDelegate: BaseAdapterInteractionDelegate<MovieModel>) :
    RecyclerView.Adapter<MovieViewHolder>() {

    private val sortedList : SortedList<MovieModel>

    init {
        sortedList = SortedList(MovieModel::class.java, object : SortedListAdapterCallback<MovieModel>(this) {
            override fun areItemsTheSame(item1: MovieModel?, item2: MovieModel?): Boolean {
                return item1?.id == item2?.id
            }

            override fun compare(o1: MovieModel?, o2: MovieModel?): Int {
                return if(o1 != null && o2 != null) o1.title.compareTo(o2.title, true) else 0
            }

            override fun areContentsTheSame(oldItem: MovieModel?, newItem: MovieModel?): Boolean {
                return oldItem.hashCode() == newItem.hashCode()
            }
        })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
            val binding = ItemListContentBinding.inflate(LayoutInflater.from(parent.context))
            return MovieViewHolder(binding).apply { interactionDelegate = adapterDelegate }
    }

    override fun getItemCount(): Int {
        return sortedList.size()
    }

    override fun getItemId(position: Int): Long {
        return sortedList[position].id
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
            holder.bind(sortedList[position])
    }

    fun updateMovieList(movieModelList: List<MovieModel>) {
        sortedList.replaceAll(movieModelList)
    }
}