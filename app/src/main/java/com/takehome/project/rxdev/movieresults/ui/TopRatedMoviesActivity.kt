package com.takehome.project.rxdev.movieresults.ui

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.ViewModelProvider
import com.takehome.project.rxdev.movieresults.R
import com.takehome.project.rxdev.movieresults.data.model.MovieModel
import com.takehome.project.rxdev.movieresults.data.model.viewmodel.MoviesViewModelFactory
import com.takehome.project.rxdev.movieresults.data.model.viewmodel.TopRatedMoviesViewModel
import com.takehome.project.rxdev.movieresults.data.repository.MovieRepository
import com.takehome.project.rxdev.movieresults.data.source.local.LocalMovieDaoProvider
import com.takehome.project.rxdev.movieresults.data.source.local.LocalRxMovieDataSource
import com.takehome.project.rxdev.movieresults.data.source.remote.RemoteMovieApi
import com.takehome.project.rxdev.movieresults.data.source.remote.RemoteRxMovieDataSource
import com.takehome.project.rxdev.movieresults.ui.adapters.delegates.BaseAdapterInteractionDelegate
import com.takehome.project.rxdev.movieresults.ui.util.fragmentfactory.MovieFragmentFactory
import java.net.UnknownHostException

class TopRatedMoviesActivity : AppCompatActivity() {

    private var fragmentTag = ""

    private val daoProvider  by lazy {
        LocalMovieDaoProvider.getRoomDatabase(applicationContext)
    }

    private val localDataSource by lazy {
        LocalRxMovieDataSource(
            daoProvider
        )
    }

    private val hasNetwork = ObservableBoolean(true)
    private val remoteApi by lazy {
        RemoteMovieApi(hasNetwork)
    }
    private val remoteDataSource by lazy {
        RemoteRxMovieDataSource(remoteApi)
    }

    private val movieRepository by lazy {
        MovieRepository(
            localDataSource,
            remoteDataSource
        )
    }

    private val networkRequest = NetworkRequest.Builder()
        /*.addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
        .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)*/
        .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
        .build()

    private val networkCallback =
        object : ConnectivityManager.NetworkCallback() {

        override fun onLost(network: Network) {
            super.onLost(network)
            hasNetwork.set(false)
        }

        override fun onUnavailable() {
            super.onUnavailable()
            hasNetwork.set(false)
        }

        override fun onAvailable(network: Network) {
            super.onAvailable(network)
            hasNetwork.set(true)
        }
    }

    private lateinit var connectivityManager: ConnectivityManager

    private lateinit var viewModel: TopRatedMoviesViewModel

    private val noConnectionToast by lazy {
        Toast.makeText(this@TopRatedMoviesActivity,
            getString(R.string.network_connection_alert),
            Toast.LENGTH_SHORT)
    }
    private val interactionDelegate by lazy {
        object :
            BaseAdapterInteractionDelegate<MovieModel> {
            override fun onAdapterItemSelected(position: Int, item: MovieModel?) {
                item?.let {
                    viewModel.getSelectedMovieDetails(item) { movieCreditModel, error ->
                        error?.let {
                            try {
                                throw error
                            } catch (e: UnknownHostException) {
                                noConnectionToast.show()
                            } catch (e : NoSuchElementException) {
                                noConnectionToast.show()
                            }
                        }
                        launchDetailFragment()
                    }
                }
            }

            override fun onAdapterItemFavorited(position: Int, item: MovieModel?) {
                viewModel.toggleFavorite(item)
            }
        }
    }

    private fun displayExistingFragment(savedInstanceState: Bundle) {
        fragmentTag = savedInstanceState.getString("fragmentTag", getActiveFragmentTag())

        val fragment =
            supportFragmentManager.findFragmentByTag(fragmentTag)
        fragment?.let {
            supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, it, fragmentTag)
                .commit()
        }
    }

    private fun displayMovieListFragment() {
        fragmentTag = "ItemListFragment"
        supportFragmentManager.beginTransaction()
            .add(
                R.id.fragment_container,
                ItemListFragment.newInstance(
                    movieRepository, interactionDelegate),
                fragmentTag)
            .commit()
    }

    private fun launchDetailFragment() {
        fragmentTag = "ItemDetailFragment"
        supportFragmentManager.beginTransaction()
            .replace(
                R.id.fragment_container,
                ItemDetailFragment
                    .newInstance(movieRepository, interactionDelegate),
                fragmentTag
            )
            .addToBackStack(fragmentTag)
            .commit()
    }

    private fun getActiveFragmentTag() : String {
        return if (supportFragmentManager.backStackEntryCount > 0)
            supportFragmentManager.getBackStackEntryAt(
                supportFragmentManager.backStackEntryCount - 1)
                .name ?: "" else "ItemListFragment"
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        connectivityManager.registerNetworkCallback(networkRequest, networkCallback)

        supportFragmentManager.fragmentFactory =
            MovieFragmentFactory(
                movieRepository,
                interactionDelegate
            )

        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_item_list)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        viewModel = ViewModelProvider(this,
            MoviesViewModelFactory(movieRepository))
            .get(TopRatedMoviesViewModel::class.java)

        savedInstanceState?.let {
            displayExistingFragment(savedInstanceState)
        } ?: displayMovieListFragment()

    }

    override fun onBackPressed() {
        viewModel.clearSelectedMovie()
        super.onBackPressed()
    }

    override fun onDestroy() {
        connectivityManager.unregisterNetworkCallback(networkCallback)
        super.onDestroy()
    }

}