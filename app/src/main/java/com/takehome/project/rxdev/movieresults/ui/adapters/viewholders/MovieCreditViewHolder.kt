package com.takehome.project.rxdev.movieresults.ui.adapters.viewholders

import androidx.databinding.ViewDataBinding
import com.takehome.project.rxdev.movieresults.BR
import com.takehome.project.rxdev.movieresults.data.model.CreditModel

class MovieCreditViewHolder<T : CreditModel>(private val binding: ViewDataBinding) : BaseViewHolder<T>(binding) {
    override fun bind(model: T?) {
        binding.setVariable(BR.movieCreditModel, model)
        binding.executePendingBindings()
    }
}