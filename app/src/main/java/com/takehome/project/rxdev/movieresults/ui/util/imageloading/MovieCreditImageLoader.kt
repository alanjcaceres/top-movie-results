package com.takehome.project.rxdev.movieresults.ui.util.imageloading

import android.widget.ImageView
import java.lang.ref.WeakReference

class MovieCreditImageLoader(private val imageView: ImageView,
                             override val imagePath : String?) : ImageVisitable {

    override val imageViewRef: WeakReference<ImageView>
        get() = WeakReference(imageView)

    override fun loadImageWith(imagePlugin: ImagePlugin) {
        imagePlugin.loadMovieCreditImage(this)
    }
}