package com.takehome.project.rxdev.movieresults.data.source.local

import com.takehome.project.rxdev.movieresults.data.model.MovieCastModel
import com.takehome.project.rxdev.movieresults.data.model.MovieCreditModel
import com.takehome.project.rxdev.movieresults.data.model.MovieCrewModel
import com.takehome.project.rxdev.movieresults.data.model.MovieModel
import io.reactivex.Single
import io.reactivex.functions.BiFunction

class LocalRxMovieDataSource(daoProvider : RoomDaoProvider) :
    LocalRxDataSource {

    private val dao = daoProvider.getLocalMovieDao()

    override fun getTopRatedMovies(): Single<List<MovieModel>>? {
        return dao.getAllMovies()
    }

    override fun getMovieCredits(movieId: Long): Single<MovieCreditModel> {
        return Single.zip(dao.getCastCredits(movieId),
            dao.getCrewCredits(movieId), BiFunction { cast, crew ->
                val model = MovieCreditModel()
                if (cast.isNotEmpty() || crew.isNotEmpty()) {
                    model.id = movieId
                }
                model.cast = cast
                model.crew = crew
                return@BiFunction model
            })
    }

    override fun getCastCredits(movieId: Long): Single<List<MovieCastModel>>? {
         return dao.getCastCredits(movieId)
    }

    override fun getCrewCredits(movieId: Long): Single<List<MovieCrewModel>>? {
        return dao.getCrewCredits(movieId)
    }

    override fun toggleFavorite(movieModel: MovieModel): Single<Int> {
        return dao.toggleFavorite(movieModel)
    }

    override fun cacheMovieModels(movieList: List<MovieModel>?): Single<List<Long>>? {
        return dao.insertMovies(movieList)
    }

    override fun cacheMovieCredits(movieCredits: MovieCreditModel): Single<List<Long>>? {
        return Single.zip(
        dao.insertCast(movieCredits.cast),
            dao.insertCrew(movieCredits.crew),
            BiFunction { cast, crew ->
                val list = arrayListOf<Long>()
                list.addAll(cast)
                list.addAll(crew)
                return@BiFunction list
            })
    }

    override fun getMovie(movieId: Long): Single<MovieModel>? {
        return dao.getMovie(movieId)
    }
}