package com.takehome.project.rxdev.movieresults.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "movie_cast_model")
class MovieCastModel :
    CreditModel {

    var movieId = -1L

    @SerializedName("cast_id")
    var castId = -1

    var character = ""

    @SerializedName("credit_id")
    var creditId = ""

    var gender : Int? = null

    @PrimaryKey
    @ColumnInfo(name = "movieCastId")
    var id = -1L

    var name = ""

    var order = -1

    @SerializedName("profile_path")
    var profilePath : String? = null


    override fun getCreditImagePath(): String? {
        return profilePath
    }

    override fun getCreditName(): String {
        return name
    }

    override fun getCreditTitle(): String? {
        return character
    }
}