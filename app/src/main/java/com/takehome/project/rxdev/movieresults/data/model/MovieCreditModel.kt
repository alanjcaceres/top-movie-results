package com.takehome.project.rxdev.movieresults.data.model

class MovieCreditModel {

    var id : Long = -1
    var cast: List<MovieCastModel> = emptyList()
    var crew :List<MovieCrewModel> = emptyList()
}