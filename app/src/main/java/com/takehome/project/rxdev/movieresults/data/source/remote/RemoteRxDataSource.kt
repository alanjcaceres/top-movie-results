package com.takehome.project.rxdev.movieresults.data.source.remote

import com.takehome.project.rxdev.movieresults.data.model.MovieCreditModel
import com.takehome.project.rxdev.movieresults.data.model.MovieModel
import io.reactivex.Single

interface RemoteRxDataSource {

    fun getTopRatedMovies() : Single<List<MovieModel>>?
    fun getMovieCredits(movieId: Long) : Single<MovieCreditModel>?

}