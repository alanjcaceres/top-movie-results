package com.takehome.project.rxdev.movieresults.data.model.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.takehome.project.rxdev.movieresults.data.repository.RxRepository

class MoviesViewModelFactory(private val movieRepository: RxRepository) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(TopRatedMoviesViewModel::class.java)){
            return TopRatedMoviesViewModel(
                movieRepository
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}