package com.takehome.project.rxdev.movieresults.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "movie_models")
class MovieModel {

    @PrimaryKey
    var id : Long = -1L

    @SerializedName("original_title")
    var originalTitle = ""

    @SerializedName("original_language")
    var originalLanguage = ""

    var title = ""

    @SerializedName("backdrop_path")
    var backdropPath : String? = null

    @SerializedName("poster_path")
    var posterPath : String? = null

    var overview = ""

    @SerializedName("release_date")
    var releaseDate = ""

    var favorited = false
}