package com.takehome.project.rxdev.movieresults.data.repository

import android.util.Log
import com.takehome.project.rxdev.movieresults.data.model.MovieCreditModel
import com.takehome.project.rxdev.movieresults.data.model.MovieModel
import com.takehome.project.rxdev.movieresults.data.source.local.LocalRxDataSource
import com.takehome.project.rxdev.movieresults.data.source.remote.RemoteRxDataSource
import io.reactivex.Single

class MovieRepository(private val localDataSource: LocalRxDataSource,
                      private val remoteDataSource: RemoteRxDataSource
) : RxRepository {

    override fun getCachedMovie(movieId: Long): Single<MovieModel>? {
        return localDataSource.getMovie(movieId)
    }

    override fun toggleFavorite(movieModel: MovieModel): Single<Int> {
        return localDataSource.toggleFavorite(movieModel)
    }

    //Top Rated Movies
    override fun getTopRatedMovies(): Single<List<MovieModel>>? {
        return Single.concat(
                getMoviesFromDB(),
                getMoviesFromNetwork())
            .filter { movieList ->
                    movieList.isNotEmpty()
                }
            .firstOrError()
    }

    private fun getMoviesFromDB(): Single<List<MovieModel>>? {
        return localDataSource.getTopRatedMovies()
    }

    private fun getMoviesFromNetwork(): Single<List<MovieModel>>? {
        return remoteDataSource.getTopRatedMovies()
            ?.doOnSuccess {
            cacheMovieModels(it)
                ?.subscribe()
        }
    }

    private fun cacheMovieModels(movieList: List<MovieModel>?) : Single<List<Long>>?{
        return localDataSource.cacheMovieModels(movieList)
    }

    //Movie Credits
    override fun getMovieCredits(movieId: Long): Single<MovieCreditModel>? {
        return Single.concat(
                getCreditsFromDB(movieId),
                getCreditsFromNetwork(movieId)
            )
                .filter { model: MovieCreditModel -> model.id != -1L }
                .firstOrError()

    }

    private fun getCreditsFromDB(movieId: Long) : Single<MovieCreditModel>? {
        return localDataSource.getMovieCredits(movieId)
    }

    private fun getCreditsFromNetwork(movieId: Long) : Single<MovieCreditModel>?{
        return remoteDataSource.getMovieCredits(movieId)
            ?.doOnSuccess {creditModel ->
            creditModel.crew.forEach { crew -> crew.movieId = movieId }
                .also { creditModel.cast.forEach { cast -> cast.movieId = movieId } }
            cacheMovieCredits(creditModel)?.subscribe()
        }
    }

    private fun cacheMovieCredits(movieCredits: MovieCreditModel): Single<List<Long>>? {
        return localDataSource.cacheMovieCredits(movieCredits)
    }
}