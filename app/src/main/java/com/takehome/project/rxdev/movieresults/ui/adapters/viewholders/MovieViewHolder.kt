package com.takehome.project.rxdev.movieresults.ui.adapters.viewholders

import androidx.databinding.ViewDataBinding
import com.takehome.project.rxdev.movieresults.BR
import com.takehome.project.rxdev.movieresults.data.model.MovieModel
import kotlinx.android.synthetic.main.item_list_content.view.*

open class MovieViewHolder(private val viewDataBinding: ViewDataBinding) : BaseViewHolder<MovieModel>(viewDataBinding) {

    override fun bind(model: MovieModel?) {
        viewDataBinding.setVariable(BR.movieModel, model)
        viewDataBinding.executePendingBindings()
        itemView.setOnClickListener {
            interactionDelegate?.onAdapterItemSelected(adapterPosition, model)
        }
        itemView.iv_heart_button.setOnClickListener {
            interactionDelegate?.onAdapterItemFavorited(adapterPosition, model)
        }

    }
}