package com.takehome.project.rxdev.movieresults.ui.util.imageloading

interface ImagePlugin {
    fun loadMovieCreditImage(imageVisitable: ImageVisitable)
    fun loadMoviePosterImage(imageVisitable: ImageVisitable)
}