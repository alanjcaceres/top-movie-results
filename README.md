## Protected Properties
There is a file call **[protected.properties](protected.properties)**. 

It contains values for the **API_BASE_URL**, **IMAGE_URL**, and **API_KEY**. 

I have removed my specific **API_KEY** as it is bad practice to include them in repositories.

You can enter your specific **API_KEY** from [The Movie Database API](https://www.themoviedb.org/documentation/api) to run the application.